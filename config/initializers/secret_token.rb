# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
DemoApp::Application.config.secret_key_base = '0d4780de0353bd1cb37519b758c8a22001eeef4c1d4679faefbb612ed08132261bddb6e87d8aef4189d300fac9fb746d316eafa2b7f866cb4c2fc89c4edcfa76'
